# Django Drafter

## Start project

```bash
cp .env.example .env
# edit .env to fit your needs
docker-compose up --build
```

### Create super user

```bash
docker-compose run --rm django python manage.py createsuperuser
```

### Reset build and DB

```bash
docker-compose down -v
```

### Migration

```bash
docker-compose run --rm django python manage.py migrate
```

## Dependencies

### Install

```bash
docker-compose run --rm django pip-compile requirements.in
```

### Update

```bash
docker-compose run --rm django pip-compile --upgrade requirements.in
```

## Heroku

Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

```bash
heroku login
```

### Add app as a GIT remote

```bash
heroku git:remote -a <heroku_app_name>
```

### Deploy on heroku server

```bash
git push heroku master
```

