from dal import autocomplete
from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

from teams.models import Player
from users.models import User


class PlayerForm(forms.ModelForm):
    player_name = forms.ModelChoiceField(
        queryset=Player.objects.all(),
        widget=autocomplete.ModelSelect2(url='player_auto_complete')
    )

    class Meta:
        model = Player
        fields = ('player_name',)


class TeamForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(
        widget=FilteredSelectMultiple(verbose_name='Users', is_stacked=False),
        queryset=User.objects.all(),
        required=False,
    )


class PickForm(forms.ModelForm):
    users = forms.ModelMultipleChoiceField(
        widget=FilteredSelectMultiple(verbose_name='Users', is_stacked=False),
        queryset=User.objects.all(),
        required=False,
    )
