from datetime import datetime

from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from teams.forms import TeamForm
from teams.models import Team, Pick, Player
from teams.resources import PlayerResource, PickResource, TeamResource


class PickAdmin(admin.TabularInline):
    model = Pick
    fk_name = 'current_team'
    extra = 0
    exclude = [
        'year',
    ]
    autocomplete_fields = [
        'player',
        'original_team',
    ]

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        current_year = datetime.now().year
        queryset = queryset.filter(year=current_year)
        return queryset


@admin.register(Team)
class TeamAdmin(ImportExportModelAdmin):
    resource_class = TeamResource
    list_display = (
        'name',
        'city',
    )
    form = TeamForm
    search_fields = [
        'name',
    ]
    inlines = [
        PickAdmin,
    ]

@admin.register(Pick)
class PickAdmin(ImportExportModelAdmin):
    resource_class = PickResource
    list_display = (
        'number',
        'round',
        'current_team',
        'original_team',
        'player',
        'picked',
        'year',
    )
    list_filter = (
        'round',
        'original_team',
        'current_team',
        'year',
    )
    autocomplete_fields = [
        'original_team',
        'current_team',
        'player',
    ]
    search_fields = [
        'original_team__name',
        'current_team__name',
        'player__name',
    ]


@admin.register(Player)
class PlayerAdmin(ImportExportModelAdmin):
    resource_class = PlayerResource
    list_display = (
        'name',
        'position',
        'overall',
        'number',
        'year',
    )
    list_filter = (
        'position',
        'year',
    )
    search_fields = [
        'name',
    ]
