import factory

from teams.models import Team, Player, Pick


class TeamFactory(factory.DjangoModelFactory):
    name = factory.Faker('pystr')
    city = factory.Faker('city')

    class Meta:
        model = Team


class PlayerFactory(factory.DjangoModelFactory):
    name = factory.Faker('name')

    class Meta:
        model = Player


class PickFactory(factory.DjangoModelFactory):
    number = factory.Sequence(lambda n: n)
    round = factory.Sequence(lambda n: n)
    original_team = factory.SubFactory(TeamFactory)
    current_team = factory.SelfAttribute('original_team')

    class Params:
        with_player = factory.Trait(
            player=factory.SubFactory(PlayerFactory),
            picked=True,
        )

    class Meta:
        model = Pick
