import json

from dal import autocomplete
from django.test import TestCase
from django.urls import reverse
from django.views import View
from django.views.generic import ListView

from teams.models import Pick
from teams.tests.factories import PlayerFactory, PickFactory
from teams.views import PickListView, PlayerAutoCompleteView, NextPickView
from users.tests.factories import UserFactory


class TestPickListView(TestCase):

    def setUp(self):
        self.user = UserFactory()
        self.client.force_login(self.user)
        self.url = reverse('pick_list')

    def test_view(self):
        self.assertTrue(issubclass(PickListView, (ListView)))
        self.assertEqual(Pick, PickListView.model)
        self.assertTupleEqual(('round', 'number',), PickListView.ordering)

    def test_get(self):
        with self.assertNumQueries(3):
            response = self.client.get(self.url)

        self.assertEqual(200, response.status_code)


class TestPlayerAutoCompleteView(TestCase):

    def setUp(self):
        self.user = UserFactory()
        self.client.force_login(self.user)
        self.url = reverse('player_auto_complete')

    def test_view(self):
        self.assertTrue(issubclass(PlayerAutoCompleteView, (autocomplete.Select2QuerySetView)))

    def test_get(self):
        with self.assertNumQueries(1):
            response = self.client.get(self.url)

        self.assertEqual(200, response.status_code)

        data = json.loads(response.content.decode('utf8'))
        self.assertEqual(0, len(data['results']))

    def test_get_with_q(self):
        PlayerFactory(name='foo')
        PlayerFactory(name='bar')

        with self.assertNumQueries(2):
            response = self.client.get(self.url, data=dict(q='oo'))

        self.assertEqual(200, response.status_code)

        data = json.loads(response.content.decode('utf8'))
        self.assertEqual(1, len(data['results']))


class TestNextPickView(TestCase):

    def setUp(self):
        self.user = UserFactory()
        self.client.force_login(self.user)
        self.url = reverse('next_pick')

    def test_view(self):
        self.assertTrue(issubclass(NextPickView, (View)))

    def test_get(self):
        PickFactory(number=1, with_player=True)
        next_pick = PickFactory(number=2)
        PickFactory(number=3)

        with self.assertNumQueries(1):
            response = self.client.get(self.url)

        self.assertEqual(200, response.status_code)

        data = json.loads(response.content.decode('utf8'))
        self.assertEqual(next_pick.number, data['pick_number'])

    def test_get_empty(self):
        PickFactory(number=1, with_player=True)

        with self.assertNumQueries(1):
            response = self.client.get(self.url)

        self.assertEqual(200, response.status_code)

        data = json.loads(response.content.decode('utf8'))
        self.assertNotIn('pick_number', data)
