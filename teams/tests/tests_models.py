from django.test import TestCase

from teams.tests.factories import TeamFactory, PlayerFactory, PickFactory


class TestTeam(TestCase):

    def test_str(self):
        team = TeamFactory()
        self.assertEqual(
            f"{team.name} {team.city}",
            str(team)
        )


class TestPlayer(TestCase):

    def test_str(self):
        player = PlayerFactory(number='', position='', overall=None)
        self.assertEqual(
            f"{player.name}",
            str(player)
        )

        player = PlayerFactory(number='1', position='', overall=None)
        self.assertEqual(
            f"#{player.number} - {player.name}",
            str(player)
        )
        player = PlayerFactory(number='', position='', overall=100)
        self.assertEqual(
            f"{player.name} ({player.overall}OV)",
            str(player)
        )

        player = PlayerFactory(number='1', position='', overall=100)
        self.assertEqual(
            f"#{player.number} - {player.name} ({player.overall}OV)",
            str(player)
        )

        player = PlayerFactory(number='1', position='G', overall=None)
        self.assertEqual(
            f"#{player.number} - {player.position} - {player.name}",
            str(player)
        )

        player = PlayerFactory(number='', position='G', overall=100)
        self.assertEqual(
            f"{player.position} - {player.name} ({player.overall}OV)",
            str(player)
        )

        player = PlayerFactory(number='1', position='G', overall=100)
        self.assertEqual(
            f"#{player.number} - {player.position} - {player.name} ({player.overall}OV)",
            str(player)
        )


class TestPick(TestCase):

    def test_str(self):
        pick = PickFactory()
        self.assertEqual(
            f"{pick.round}/{pick.number} {pick.current_team}",
            str(pick)
        )

