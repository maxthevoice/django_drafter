from datetime import datetime

from dal import autocomplete
from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from django.views.generic import ListView

from teams.forms import PlayerForm
from teams.models import Pick, Player


class PickListView(ListView):
    model = Pick
    ordering = ('round', 'number',)

    def get_queryset(self):
        current_year = datetime.now().year
        return self.model.objects.filter(year=current_year)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = PlayerForm()
        return context


class PlayerAutoCompleteView(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        current_year = datetime.now().year
        qs = Player.objects.filter(pick__isnull=True, year=current_year)

        if self.q:
            qs = qs.filter(Q(name__icontains=self.q))

        return qs


class NextPickView(View):

    @staticmethod
    def get(request):
        current_year = datetime.now().year
        pick = Pick.objects.filter(picked=False, year=current_year).order_by('number').first()
        data = dict()
        if pick:
            data['pick_number'] = pick.number
        return JsonResponse(data)
