from django.urls import path

from teams.consumers import PickConsumer

websocket_urlpatterns = [
    path('ws/pick/', PickConsumer),
]
