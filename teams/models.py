import uuid
from datetime import datetime

from django.db import models

from users.models import User

def current_year():
    return datetime.now().year


class Team(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    image = models.URLField(max_length=300, null=True, blank=True)
    users = models.ManyToManyField(User)

    def __str__(self):
        return f"{self.city} {self.name}"

    class Meta:
        ordering = ['city', 'name']


class Player(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=250)
    position = models.CharField(max_length=100, blank=True)
    overall = models.IntegerField(null=True, blank=True)
    number = models.CharField(max_length=250, blank=True)
    year = models.CharField(max_length=4, default=current_year)

    def __str__(self):
        if self.number and self.position and self.name and self.overall:
            return f"#{self.number} - {self.position} - {self.name} ({self.overall}OV)"
        if not self.number and self.position and self.name and self.overall:
            return f"{self.position} - {self.name} ({self.overall}OV)"
        if self.number and not self.position and self.name and self.overall:
            return f"#{self.number} - {self.name} ({self.overall}OV)"
        if self.number and self.position and self.name and not self.overall:
            return f"#{self.number} - {self.position} - {self.name}"
        if not self.number and not self.position and self.name and self.overall:
            return f"{self.name} ({self.overall}OV)"
        if self.number and not self.position and self.name and not self.overall:
            return f"#{self.number} - {self.name}"
        return f"{self.name}"

    class Meta:
        ordering = ['name']


class Pick(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    number = models.SmallIntegerField()
    round = models.SmallIntegerField()
    original_team = models.ForeignKey(Team, on_delete=models.PROTECT, related_name='original_team')
    current_team = models.ForeignKey(Team, on_delete=models.PROTECT, related_name='current_team')
    player = models.OneToOneField(Player, null=True, blank=True, on_delete=models.PROTECT)
    picked = models.BooleanField(default=False)
    year = models.CharField(max_length=4, default=current_year)

    class Meta:
        unique_together = [('number', 'round', 'year'), ('round', 'original_team', 'year')]
        ordering = ['number']

    def __str__(self):
        return f"{self.round}/{self.number} {self.current_team}"
