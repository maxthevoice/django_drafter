from import_export import resources, fields
from import_export.widgets import ForeignKeyWidget

from teams.models import Player, Team, Pick


class PlayerResource(resources.ModelResource):
    class Meta:
        model = Player
        fields = ('id', 'name', 'position', 'overall', 'number')


class TeamResource(resources.ModelResource):
    class Meta:
        model = Team
        fields = ('id', 'name', 'city', 'image',)


class PickResource(resources.ModelResource):
    original_team = fields.Field(
        column_name='original_team',
        attribute='original_team',
        widget=ForeignKeyWidget(Team, 'name'))
    current_team = fields.Field(
        column_name='current_team',
        attribute='current_team',
        widget=ForeignKeyWidget(Team, 'name'))
    player = fields.Field(
        column_name='player',
        attribute='player',
        widget=ForeignKeyWidget(Player, 'name'))

    class Meta:
        model = Pick
        fields = ('id', 'number', 'round', 'original_team', 'current_team', 'player', 'picked')
