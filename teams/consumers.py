from datetime import datetime

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
import json

from teams.models import Pick, Player


class PickConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add(
            'draft',
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            'draft',
            self.channel_name
        )

    async def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        pick_number = text_data_json['pick_number']
        player_id = text_data_json['player_id']
        player_name = text_data_json['player_name']

        await self.save_pick_player(pick_number, player_id)

        # Send message to room group
        await self.channel_layer.group_send(
            'draft',
            {
                'type': 'pick_player',
                'pick_number': pick_number,
                'player_name': player_name,
            }
        )

    @database_sync_to_async
    def save_pick_player(self, pick_number, player_id):
        current_year = datetime.now().year
        user = self.scope["user"]
        queryset = Pick.objects.filter(number=pick_number, year=current_year)
        if not user.is_staff:
            queryset = queryset.filter(current_team__users__in=[user])
        queryset.update(
            player_id=player_id,
            picked=True,
        )

    async def pick_player(self, event):
        pick_number = event['pick_number']
        player_name = event['player_name']

        await self.send(text_data=json.dumps({
            'pick_number': pick_number,
            'player_name': player_name,
        }))
