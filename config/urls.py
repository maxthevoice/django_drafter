from django.contrib import admin
from django.urls import path, include

from teams.views import PickListView, PlayerAutoCompleteView, NextPickView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', PickListView.as_view(), name='pick_list'),
    path('players-auto-complete/', PlayerAutoCompleteView.as_view(), name='player_auto_complete'),
    path('next-pick/', NextPickView.as_view(), name='next_pick'),
]
