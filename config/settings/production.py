import dj_database_url

from .base import *  # noqa
from .base import env

DEBUG = TEMPLATE_DEBUG = False  # noqa F405

DATABASES = {
    'default': dj_database_url.config(
        default=env.str('DATABASE_URL')
    )
}

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [env.str('REDIS_URL')],
        },
        "symmetric_encryption_keys": [SECRET_KEY],
    },
}
