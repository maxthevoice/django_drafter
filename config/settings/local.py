# noinspection PyUnresolvedReferences
from .base import *
from .base import env

DEBUG = TEMPLATE_DEBUG = env.bool('DEBUG', True)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env.str('POSTGRES_DB'),
        'USER': env.str('POSTGRES_USER'),
        'PASSWORD': env.str('POSTGRES_PASSWORD'),
        'HOST': env.str('POSTGRES_HOST', 'postgres'),
        'PORT': 5432,
    },
}

