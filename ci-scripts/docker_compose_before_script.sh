#!/usr/bin/env sh

set -e
set -x

apk add --update --no-cache py-pip
pip install \
  docker-compose==1.23.2 \
  awscli
set +x
$(aws ecr get-login --no-include-email --region ca-central-1)
set -x
cp .env.ci .env
