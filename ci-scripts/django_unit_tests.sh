#!/usr/bin/env bash

set -e
set -x

cp .env.ci .env
export $(cat .env | xargs)
python manage.py collectstatic --noinput --verbosity 0
coverage run manage.py test
set +e
coverage report; export coverage_result=$?
coverage html
set -e
exit ${coverage_result}
