#!/usr/bin/env bash

set -e
set -x

apt update
apt install -y python-dev graphviz libgraphviz-dev pkg-config
pip install pygraphviz
cp .env.ci .env
export $(cat .env | xargs)
python manage.py graph_models -a -g -o $1
