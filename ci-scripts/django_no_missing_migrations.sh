#!/usr/bin/env bash

set -e
set -x

cp .env.ci .env
export $(cat .env | xargs)
python manage.py makemigrations --check --dry-run
