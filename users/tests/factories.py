import factory
from faker import Faker

from users.models import User

fake = Faker()


class UserFactory(factory.DjangoModelFactory):
    username = factory.LazyAttribute(lambda a: 'user-{}'.format(fake.pystr()).lower())
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.LazyAttribute(lambda a: '{}@example.com'.format(fake.pystr()).lower())
    password = factory.PostGenerationMethodCall('set_password', 'password')

    class Params:
        super_user = factory.Trait(
            is_superuser=True,
            is_staff=True,
            is_active=True,
        )

    class Meta:
        model = User
